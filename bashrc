#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# EOS Stuff         {{{1
[[ -f ~/.welcome_screen ]] && . ~/.welcome_screen

_set_liveuser_PS1() {
    PS1='[\u@\h \W]\$ '
    if [ "$(whoami)" = "liveuser" ] ; then
        local iso_version="$(grep ^VERSION= /usr/lib/endeavouros-release 2>/dev/null | cut -d '=' -f 2)"
        if [ -n "$iso_version" ] ; then
            local prefix="eos-"
            local iso_info="$prefix$iso_version"
            PS1="[\u@$iso_info \W]\$ "
        fi
    fi
}
_set_liveuser_PS1
unset -f _set_liveuser_PS1

ShowInstallerIsoInfo() {
    local file=/usr/lib/endeavouros-release
    if [ -r $file ] ; then
        cat $file
    else
        echo "Sorry, installer ISO info is not available." >&2
    fi
}
#   }}}

[[ "$(whoami)" = "root" ]] && return

[[ -z "$FUNCNEST" ]] && export FUNCNEST=100          # limits recursive functions, see 'man bash'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

##### More EOS Stuff ########## {{{
################################################################################
## Some generally useful functions.
## Consider uncommenting aliases below to start using these functions.
## October 2021: removed many obsolete functions. If you still need them, please look at
## https://github.com/EndeavourOS-archive/EndeavourOS-archiso/raw/master/airootfs/etc/skel/.bashrc
_open_files_for_editing() {
    # Open any given document file(s) for editing (or just viewing).
    # Note1:
    #    - Do not use for executable files!
    # Note2:
    #    - Uses 'mime' bindings, so you may need to use
    #      e.g. a file manager to make proper file bindings.

    if [ -x /usr/bin/exo-open ] ; then
        echo "exo-open $@" >&2
        setsid exo-open "$@" >& /dev/null
        return
    fi
    if [ -x /usr/bin/xdg-open ] ; then
        for file in "$@" ; do
            echo "xdg-open $file" >&2
            setsid xdg-open "$file" >& /dev/null
        done
        return
    fi

    echo "$FUNCNAME: package 'xdg-utils' or 'exo' is required." >&2
}

#------------------------------------------------------------
## Aliases for the functions above.
## Uncomment an alias if you want to use it.

# alias ef='_open_files_for_editing'     # 'ef' opens given file(s) for editing
# alias pacdiff=eos-pacdiff
######################################################################   }}}

##### My Stuff ########## 
# last update: 2022-02-22 - Changed escape chars in colour variables from '\e'
# to \001 and \002. Seems to fix terminal input from overwriting itself and weird
# behavior from editing commands in bash history.

# colour prompt!    {{{
# default at install:   [\u@\h \W]\$
RESET="\001$(tput sgr0)\002"
DGREEN="\001$(tput setaf 2)\002"
GREEN="\001$(tput setaf 40)\002"
GOLD="\001$(tput setaf 178)\002"
CYAN="\001$(tput setaf 87)\002"
TURQ="\001$(tput setaf 42)\002"

if [ $TERM == "xterm-kitty" ] ; then
    PS1="\[\033[32m\][\u@\h \[\033[33m\]\W]\$\[\033[0m\] "
else
    PS1="${TURQ}[\u@\h ${GOLD}\W${TURQ}]${RESET}\$ "
    #PS1="<OSC>133;A<ST>${TURQ}[\u@\h ${GOLD}\W${TURQ}]${RESET}\$ "
fi
export PS1

# export PS1="${TURQ}[\u@\h ${GOLD}\W${TURQ}]${RESET}\$ "

unset RESET
unset DGREEN
unset GREEN
unset GOLD
unset CYAN
unset TURQ

# enable color support of ls and also add handy aliases
# if [ -x /usr/bin/dircolors ]; then
#     test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
#     alias ls='ls --color=auto'
#     alias dir='dir --color=auto'
#     alias vdir='vdir --color=auto'
# 
#     alias grep='grep --color=auto'
#     alias fgrep='fgrep --color=auto'
#     alias egrep='egrep --color=auto'
# fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
################################ }}}

cowfortune

# make vim the default editor!
export VISUAL=vim
export EDITOR="$VISUAL"

# Alias definitions
# definitions can be found in the file:
# ~/.bash_aliases

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; 
then 
    source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION

# the fuck 
eval $(thefuck --alias)

RANGER_LOAD_DEFAULT_RC=FALSE

# vim:foldmethod=marker:ts=4:ft=sh:
