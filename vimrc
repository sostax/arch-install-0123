" My copy of an example vimrc file.
"
" copied from:	Bram Moolenaar <Bram@vim.org>
" Last change:	2022-03-13
"
" When started as "evim", evim.vim will already have done these settings, bail
" out.


" Copied from example vimrc     {{{
if v:progname =~? "evim"
  finish
endif

" Get the defaults that most users want.
if !has('nvim')
    source $VIMRUNTIME/defaults.vim
endif

if has("vms")
  set nobackup  " do not keep a backup file, use versions instead
else
  set backup    " keep a backup file (restore to previous version)
  if has('persistent_undo') && !has('nvim')
    set undofile  " keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif


" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif
""""""""""""""""""""""""""""""""""""""""""}}}

" My own settings   {{{
"
syntax on           " highlight syntax, of course
set nocompatible    " don't do wierd vi stuff
filetype plugin on  " allow for autocmds to be run based on filetype
filetype indent on  " as recommended by vimtex
set encoding=utf8   " same as above
set termguicolors
set number          " show line numbers, AND...
set rnu             " show relative line numbers

" briefly show matching parens while typing
set showmatch matchtime=3

" highlight line under the cursor
set cursorline     

""""""""""""""""""""""""""""""""""""""""""}}}

" Only do this part when compiled with support for autocommands
if has("autocmd")
    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx
        au!
        " For all text files set 'textwidth' to 78 characters.
        autocmd FileType text setlocal textwidth=78
	augroup END
else
		set autoindent		" always set autindenting on
	endif "has("autocmd")

    " TODO: skeletons!
    " augroup java_group
    "     au!
    "     au FileType     java    setlocal textwidth=120 sw=4 ts=4 expandtab
    "     au BufNewFile   *.java  0r ~/vim/skele/skeleton.java
    "     au BufRead      *.java  setlocal 
    " augroup END

    " Startup in Netrw   {{{
    " https://stackoverflow.com/a/63239425 
    " Checks if vim started up with a file,
    " if not, opens current working directory in Netrw }}}
    " augroup InitNetrw
    "     autocmd!
    "     autocmd VimEnter * if expand("%") == "" | edit . | endif
    " augroup END

    " augroup mkFiles
    "     au!
    "     autocmd  BufEnter   ?akefile*        set include=^s\=include
    "     autocmd  BufLeave   ?akefile*        set include& 
    "     autocmd  BufNewFile ?akefile*        0r ~/vim/skel/makeskel
    " augroup END 

    " Read a skeleton template file when opening C, C++, makefiles
    " .c / .cpp / .h
    " skeletons TODO: augroup this!
    " autocmd  BufNewFile *.c*             0r ~/vim/skel/skeleton.c
    " autocmd  BufNewFile *.h              0r ~/vim/skel/skeleton.h
    " autocmd  BufRead    *.c,*.cpp,*.h    1;/^{    









" Improve search & highlight {{{
" highlight search
set hlsearch
" incremental search while typing
set incsearch
" easily clear highlighting for search terms please, it's annoying
noremap <silent> <C-_> :nohlsearch<CR>
inoremap <silent> <C-_> <C-o>:nohlsearch<CR>

""""""""""""""""""""""""""""""""""""""""""}}}

" important commands from default   {{{

" allow backspace over more things
set backspace=indent,eol,start

" show multi-keystroke cmds in bottom right
set showcmd

" nicer tabbing in cmd mode
set wildmenu
" 1st tab: complete longest match, bring up wildmenu
" additional tabs cycle through menu
set wildmode=longest:full,full

""""""""""""""""""""""""""""""""""""""""""}}}

" other settings {{{

set tags+=./tags,tags;	" love ctags
set splitright		" new splits open to the right or bottom
set splitbelow		" same
set nojoinspaces	" don't put 2 spaces after [.!?]
set tabstop=4		" set \t = 4
set shiftwidth=4	" level of whitespace to indent
set softtabstop=4   " delete 4 spaces with backspace over tabs
set smarttab		" be smart about tabs
"set autoindent
set complete-=i
set expandtab		" use spaces for tabs -> disabled for now
"set t_Co=256		" use ALL OF THE COLOURS
set colorcolumn=79	" set visual mark at 79th column
set scrolloff=7		" scroll the page when 7 from top/bottom

" safe space for backups, swap, and undo
set backupdir=~/.cache/vim/backup	" where the backups live
set directory=~/.cache/vim/swap		" where the swaps live
set undodir=~/.cache/vim/undo  		" where the undo files live

" add an extension to backups
au BufWritePre * let &bex = '-' . '%:.' . '~'

" make sure nobody messes with c++ tab sizes
autocmd Filetype cpp setlocal tabstop=4

" make little lines to show indent level
"set list lcs=tab:\|\ 
""""""""""""""""""""""""""""""""""""""""""}}}

" from tpope {{{

" Delete comment character when joining commented lines
if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j
endif

if empty(mapcheck('<C-U>', 'i'))
  inoremap <C-U> <C-G>u<C-U>
endif
if empty(mapcheck('<C-W>', 'i'))
  inoremap <C-W> <C-G>u<C-W>
endif

""""""""""""""""""""""""""""""""""""""""""}}}

" misc mappings     {{{

imap ii <Esc>

" use Y like C and D to yank to end of line
nmap Y y$

" use <F3> to jump to next window
"nmap <F3> <C-w><C-w>
"imap <F3> <C-o> <C-w><C-w>
"
" F5 saves, runs make, opens QuickFix
nmap <F5> :w <CR> :make <CR>:copen <CR>
imap <F5> <ESC>:w <CR> :make<CR>:copen <CR>

" F6 opens a terminal runs the executable (when named the same as the file)
nmap <F6> :execute "terminal" expand("%:h)

" cmd to get vimgrep to make QuickFix list of TODO stuff!
command Todo noautocmd vimgrep /TODO\|FIXME/j ** | cw

" unload all buffers except the current one
nmap ,bd :execute ".+,$bd" <CR>



"""""""""""""""""""""""""""""""""""""""""" }}}

" compiling  {{{

" for single file projects: g++ compiler with the right flags - creates exe with file's name
"set makeprg=g++\ -Wall\ -Wconversion\ -ansi\ -pedantic-errors\ %:t\ -o\ %:t:r\
" run with F5 from n or i mode [see mappings above for shortcuts]
set makeprg=make\ all

" save, compile, and run file in the term with the tap of two buttons
"set shellcmdflag=-ic

""""""""""""""""""""""""""""""""""""""""""}}}

" vim-plug      {{{
call plug#begin('~/.vim/plugged')

" themes
Plug 'sainnhe/everforest'
Plug 'vv9k/bogster'
Plug 'kartik1998/simba.vim'
" tools & features
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'chrisbra/Colorizer'
Plug 'lervag/vimtex'
Plug 'bfrg/vim-cpp-modern'
Plug 'Yggdroot/indentLine'

call plug#end()
""""""""""""""""""""""""""""""""""""""""""}}}

" Yggdroot/indentLine    {{{
set conceallevel=1
let g:indentLine_conceallevel=1
"let g:indentLine_char_list = ['|', '¦', '┆', '┊']
let g:indentLine_char_list = ['┆', '┊']
let g:indentLine_showFirstIndentLevel=0 
let g:indentLine_bufTypeExclude = ['help', 'terminal']
" g:indentLine_fileTypeExclude = [ ]
""""""""""""""""""""""""""""""""""""""""""}}}


" Colour Theme settings         {{{
" everforest    {{{
" Important!!
"if has('termguicolors')
"	set termguicolors
"endif
"" For dark version.
"set background=dark
"" Set contrast.
"" This configuration option should be placed before `colorscheme everforest`.
"" Available values: 'hard', 'medium'(default), 'soft'
"let g:everforest_background = 'hard'
"
"colorscheme everforest
"
"let g:airline_theme = 'everforest'

""""""""""""""""""""""}}}

" bogster       {{{
if has('termguicolors')
    set termguicolors
endif

" for dark version
set background=dark

colorscheme bogster
let g:airline_theme = 'bogster'

" make fold lines darker!
hi! link Folded HintMsg
hi! link IncSearch Visual
hi! link Search HintMsgInverse
hi! link Identifier Special
hi! link cMemberAccess Special
hi! link Structure Special
hi! link cTypeDef BogsterYellow
" hi! link Conceal 

" make statusline darker
" changed actual colour codes in the bogster airline theme located at
" ~/.vim/plugged/bogster/autoload/airline/themese/bogster.vim
" backup created in ~/vim-backup

" bogster theme has been edited to set comments italic, somehow this broke
" ColorColumn, making it red. It is relinked here to match CursorLine
hi! link ColorColumn CursorLine
"""""""""""""""""""""" }}}

" vim-cpp-modern    {{{
let g:cpp_member_highlight = 1
""""""""""""""""""""""""""""""""""""""""""}}}

""""""""""""""""""""""""""""""""""""""""""}}}

" airline settings      {{{

let g:airline#extensions#whitespace#enabled = 0
let g:airline_powerline_fonts = 1
"let g:airline_left_sep = ''
"let g:airline_left_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_alt_sep = ''
"let g:airline_symbols.colnr = ' :'
"let g:airline_symbols.readonly = ''
"let g:airline_symbols.linenr = ' :'
"let g:airline_symbols.maxlinenr = '☰ '
"let g:airline_symbols.dirty='⚡'

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#vimtex#enabled = 1
let g:airline#extensions#vimtex#compiled = "c₁"
let g:airline#extensions#vimtex#continuous = "c"

"""""""""""""""""""""" }}}

" vimtex    {{{

autocmd FileType tex setlocal conceallevel=0
autocmd VimEnter *.tex       setlocal conceallevel=0
" This is necessary for VimTeX to load properly. The "indent" is optional.
" Note that most plugin managers will do this automatically.
" filetype plugin indent on " commented out because it's already on

" This enables Vim's and neovim's syntax-related features. Without this, some
" VimTeX features will not work (see ":help vimtex-requirements" for more
" info).
" syntax enable " commented out because it's already on

" Viewer options: One may configure the viewer either by specifying a built-in
" viewer method:
let g:vimtex_view_method = 'zathura'

" VimTeX uses latexmk as the default compiler backend. If you use it, which is
" strongly recommended, you probably don't need to configure anything. If you
" want another compiler backend, you can change it as follows. The list of
" supported backends and further explanation is provided in the documentation,
" see ":help vimtex-compiler".
" let g:vimtex_compiler_method = 'latexrun'

" Most VimTeX mappings rely on localleader and this can be changed with the
" following line. The default is usually fine and is the symbol "\".
" let maplocalleader = ","      }}}

" vim:ts=4:fdm=marker

