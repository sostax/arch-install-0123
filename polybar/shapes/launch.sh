#!/usr/bin/env bash

# Add this script to your wm startup file.

DIR="$HOME/.config/polybar/shapes"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar
polybar -q right -c "$DIR"/config.ini &
polybar -q left -c "$DIR"/config.ini &

# below is the code to make these scripts have multi monitor support 
# then just add monitor = ${env:MONITOR} to the config.ini for the bar
# src: https://github.com/adi1090x/polybar-themes/issues/153
# if type "xrandr" > /dev/null; then
#     while read F1 F2 _; do
#         if [ "$F2" = 'connected' ]; then
#             MONITOR=$F1 polybar --reload main -c ~/.config/polybar/shapes/config.ini &
#         fi
#     done <<< $( xrandr )
# else
#     polybar --reload main -c ~/.config/polybar/shapes/config.ini  &
# fi
