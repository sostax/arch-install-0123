#!/usr/bin/env bash

DIR="$HOME/.config/polybar/blocks"

# kill existing bars
killall -q polybar

# let them die in peave
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# launch just one bar
polybar -q main -c "$DIR"/oneconfig.ini &
