# ~/.bash_aliases
# collection of custom aliases
#
# Alias Definitions


alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
                                 
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias ls='ls --color=auto --si --group-directories-first'
alias ll='ls -lX'
alias lal='ls -AlhpX'
alias ld='ls -ldp'
alias l.='ls .*'
alias lr='ls -RplX'


alias j='jobs -l'

alias doc='cd ~/Documents/'
alias docs='cd ~/Documents/'
alias config='cd ~/.config/'
alias school='cd ~/Documents/ze-vault/school/'
alias vault='cd ~/Documents/ze-vault/'
alias asg='cd ~/haskoli/assignments/'

# helpful aliases for tree to limit heaps of garbage
alias tree='tree -I "bin|lib|include|share|doc" --filelimit 20 --dirsfirst'
alias treed='tree -d'
alias treepp='tree -P "*.cpp|*.h|*.o" --dirsfirst --prune'

# for connecting to INS server
alias ins-vpn='xfce4-terminal --tab -H -T vpn -x sudo openvpn --config ~/.config/macovpn-config.Fall2020.ovpn'
alias ins-ssh='xfce4-terminal --tab -H -T ssh -x ssh sosta596@ins.mtroyal.ca'

cpr() {
    echo ----------------------------------------------------------------------
    printf "CONFIRM COMMAND:\n\n"
    echo rsync -rvh --stats --info=progress2 sosta596@ins.mtroyal.ca:"$@"
    echo
    echo "***********************************"
    printf "GETTING:\t%s \nFROM:\t\t%s\n" $1 $2
    echo "***********************************"
    echo
    printf "Hit 'CTRL-C' to to cancel!\nYou have 5 seconds to decide...\n\n"
    sleep 1
    printf "5"
    for i in {4..1..-1} ;
    do
        printf " . . . %d" $i
        sleep 1
    done
    echo
    rsync -rvh --stats --info=progress2 sosta596@ins.mtroyal.ca:"$@"
    echo ----------------------------------------------------------------------
} 

mvr () {
    echo ----------------------------------------------------------------------
    printf "CONFIRM COMMAND:\n\n"
    echo rsync -rvh --stats --info=progress2 "$1" sosta596@ins.mtroyal.ca:"$2"
    echo
    echo "***********************************"
    printf "SENDING:\t%s \nTO:\t\t%s\n" $1 $2
    echo "***********************************"
    echo
    printf "Hit 'CTRL-C' to to cancel!\nYou have 5 seconds to decide...\n\n"
    sleep 1
    printf "5"
    for i in {4..1..-1} ;
    do
        printf " . . . %d" $i
        sleep 1
    done
    echo
    rsync -rvh --stats --info=progress2 "$1" sosta596@ins.mtroyal.ca:"$2"
    echo ----------------------------------------------------------------------
} 
    
# reminder to use trash-cli instead of rm
alias rm='echo "Do you mean trash-put?? use a backslash for rm if you really want"; false'
alias trm='trash'

# type less to connect to protonvpn-cli
alias vpnp='protonvpn-cli'

# user-defined functions (many shamelessly borrowed from 
# /usr/share/doc/bash/examples/ )
#connect-ins () {
#    xfce4-terminal --tab -x sudo openvpn --config .config/macovpn-config.Fall2020.ovpn -T openvpn
#}

# creates a directory and then cds into it
# usage: mcd <new-dir-name>
mkcd () {
    mkdir -p "$1"
    cd "$1"
}


# marco will save your current directory, polo will take you back there
marco () {
    LAST=$(pwd)
    echo "saved! type \`polo\` to return to $(pwd) from anywhere."
}


polo () {
    cd "$LAST"
    echo "you are now back in $(pwd)"
}

# adds an alias to the current shell and saves it to ~/.bash_aliases
add-alias () {
    local name=$1 value="$2"
    echo "alias $name=\'$value\'"
    echo alias $name=\'$value\' >>~/.bash_aliases
    eval alias $name=\'$value\'
    alias $name
}

# wifi-on {{{
wifi-on () { 
    echo 
    echo ------------------------------------------------------------
    echo ------------------------------------------------------------
    echo 
    echo "Turning on 'radio wifi' with nmcli"
    nmcli radio wifi on
    sleep 0.5
    echo "Connecting to UnimatrixZero. . . ."
    nmcli device wifi connect UnimatrixZero
    echo "Please wait. . ."
    sleep 3
    nmcli device
    echo
    echo ------------------------------------------------------------
    echo
    echo "██╗    ██╗██╗███████╗██╗     ██████╗ ███╗   ██╗"
    echo "██║    ██║██║██╔════╝██║    ██╔═══██╗████╗  ██║"
    echo "██║ █╗ ██║██║█████╗  ██║    ██║   ██║██╔██╗ ██║"
    echo "██║███╗██║██║██╔══╝  ██║    ██║   ██║██║╚██╗██║"
    echo "╚███╔███╔╝██║██║     ██║    ╚██████╔╝██║ ╚████║"
    echo " ╚══╝╚══╝ ╚═╝╚═╝     ╚═╝     ╚═════╝ ╚═╝  ╚═══╝"
    echo "                                               "
    echo ------------------------------------------------------------
    echo 
    echo "To disconnect: wifi-off"
    echo "For help, try: nmcli help"
    echo
    echo ------------------------------------------------------------
    echo ------------------------------------------------------------
    echo
}
# }}}

# wifi-off {{{
wifi-off () {
    echo 
    echo ------------------------------------------------------------
    echo ------------------------------------------------------------
    echo 
    echo "Disconnecting wifi. . . "
    echo
    nmcli device disconnect wlan0
    echo
    echo "Disconnected wifi."
    nmcli device
    echo
    echo --------------------------------------------------------------------------------------
    echo
    echo "  ▄████  ▒█████   ▒█████  ▓█████▄  ▄▄▄▄ ▓██   ██▓▓█████     █     █░ ██▓  █████▒██▓"
    echo " ██▒ ▀█▒▒██▒  ██▒▒██▒  ██▒▒██▀ ██▌▓█████▄▒██  ██▒▓█   ▀    ▓█░ █ ░█░▓██▒▓██   ▒▓██▒"
    echo "▒██░▄▄▄░▒██░  ██▒▒██░  ██▒░██   █▌▒██▒ ▄██▒██ ██░▒███      ▒█░ █ ░█ ▒██▒▒████ ░▒██▒"
    echo "░▓█  ██▓▒██   ██░▒██   ██░░▓█▄   ▌▒██░█▀  ░ ▐██▓░▒▓█  ▄    ░█░ █ ░█ ░██░░▓█▒  ░░██░"
    echo "░▒▓███▀▒░ ████▓▒░░ ████▓▒░░▒████▓ ░▓█  ▀█▓░ ██▒▓░░▒████▒   ░░██▒██▓ ░██░░▒█░   ░██░"
    echo " ░▒   ▒ ░ ▒░▒░▒░ ░ ▒░▒░▒░  ▒▒▓  ▒ ░▒▓███▀▒ ██▒▒▒ ░░ ▒░ ░   ░ ▓░▒ ▒  ░▓   ▒ ░   ░▓  "
    echo "  ░   ░   ░ ▒ ▒░   ░ ▒ ▒░  ░ ▒  ▒ ▒░▒   ░▓██ ░▒░  ░ ░  ░     ▒ ░ ░   ▒ ░ ░      ▒ ░"
    echo "░ ░   ░ ░ ░ ░ ▒  ░ ░ ░ ▒   ░ ░  ░  ░    ░▒ ▒ ░░     ░        ░   ░   ▒ ░ ░ ░    ▒ ░"
    echo "      ░     ░ ░      ░ ░     ░     ░     ░ ░        ░  ░       ░     ░          ░  "
    echo "                           ░            ░░ ░                                       "
    echo --------------------------------------------------------------------------------------
    echo ------------------------------------------------------------
    echo 
    echo "To turn wifi back on: wifi-on"
    echo "For help, try: nmcli help OR nmcli device help"
    echo
    echo ------------------------------------------------------------
    echo ------------------------------------------------------------
    echo 
}
# }}}

# print & printer-off {{{
printer () {
    echo
    echo "--------------------------------------------------"
    echo "______      ___________                           "
    echo "___  /_________  /__  /_____                      "
    echo "__  __ \  _ \_  /__  /_  __ \                     "
    echo "_  / / /  __/  / _  / / /_/ /                     "
    echo "/_/ /_/\___//_/  /_/  \____/                      "
    echo "               _____       _____            ______"
    echo "__________________(_)________  /_______________  /"
    echo "___  __ \_  ___/_  /__  __ \  __/  _ \_  ___/_  / "
    echo "__  /_/ /  /   _  / _  / / / /_ /  __/  /    /_/  "
    echo "_  .___//_/    /_/  /_/ /_/\__/ \___//_/    (_)   "
    echo "/_/                                               "
    echo "--------------------------------------------------"
    echo 
    echo 'Is wifi already on?  y or n: '
    read yorn
    if [[ "$yorn" == "n" || "$yorn" == "N" ]] ; then 
        echo Starting wifi . . . 
        wifi-on
    fi
    echo
    echo Starting cups . . . 
    cupsenable
    echo cupsenable
    sleep 1
    systemctl start cups.service
    echo systemctl start cups.service
    sleep 2
    echo Do you want to see lpinfo output?
    read yorn
    if [[ "$yorn" == "y" || "$yorn" == "Y" ]] ; then 
        lpinfo -v | tail -5
    fi
    echo ------------------------------------------------------------
    echo
    echo Printer enabled
    echo CUPS web interface: http://localhost:631/
    echo
    echo ------------------------------------------------------------
    echo ------------------------------------------------------------
}

printer-off () {
    echo
    echo -----------------------------------------------
    echo -----------------------------------------------
    echo "______                                       "
    echo "___  /______  ______                         "
    echo "__  __ \_  / / /  _ \                        "
    echo "_  /_/ /  /_/ //  __/                        "
    echo "/_.___/_\__, / \___/                         "
    echo "       /____/  _____       _____             "
    echo "__________________(_)________  /_____________"
    echo "___  __ \_  ___/_  /__  __ \  __/  _ \_  ___/"
    echo "__  /_/ /  /   _  / _  / / / /_ /  __/  /    "
    echo "_  .___//_/    /_/  /_/ /_/\__/ \___//_/     "
    echo "/_/                                          "
    echo -----------------------------------------------
    echo -----------------------------------------------
    echo
    echo Stopping cups.service
    systemctl stop cups.service
    echo Disabling cups
    cupsdisable
    echo
    echo -----------------------------------------------
    echo
    echo Printing capabilities turned off.
    echo
    echo -----------------------------------------------
    echo -----------------------------------------------
}
# }}}


nms-editor () {
    wmname compiz
    java -jar ~/Desktop/NMSsaveeditor/NMSSaveEditor.jar
}

alias launch-bar='/home/saturn/.config/polybar/blocks/launch.sh'

singlemon () {
    echo "***********************************"
    echo
    echo "goodbye HDMI!"
    ~/.screenlayout/onemonitor.sh
    /home/saturn/.config/polybar/blocks/justone.sh
    echo "re-launched polybar with config: blocks/oneconfig.ini"
    echo
    echo "***********************************"
}
   
dualmon () {
    ~/.screenlayout/monitor.sh
    sleep 3
    echo "***********************************"
    echo
    echo "hi HDMI!"
    /home/saturn/.config/polybar/blocks/launch.sh
    sleep 2
    echo "re-launched polybar with config: blocks/config.ini"
    echo
    echo "***********************************"

}
alias hatari='hatari -m -w'
